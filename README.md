# README #

Clone this repository. The system should have python3 and lboost filesystem module for C++.

### What is this repository for? ###

Distributed database wrapper over any database file system.

### How do I get set up? ###

* Clone repo
* Run the command 'g++ DS_BufferManager.cc DS_FileHandle.cc DS_PageHandle.cc DS_RemoteManager.cc DS_Manager.cc dist2.cc -o dist2 -lboost_system -lboost_thread -lpthread -lboost_filesystem -std=c++11' 
* Change dist2 to diststore and run the above command to get 'diststore' executable
* dist2 is to serve the files and diststore is to retrieve the files
* run python3 named.py to start the named server
* Specifify IP address of named server in 'DS.h' and 'named.py'
* Specify IP address,port and file location of database files in 'nameserverdict' in named server machine

### Who do I talk to? ###

* menonsandu@gmail.com
* akurathiyeshwanth@gmail.com