#include <iostream>
#include <fstream>
#include <stdio.h>
#include <cstdlib>
#include <thread>
#include <utility>
#include <stdlib.h>
#include <fcntl.h>   /* For O_RDWR */
#include <unistd.h>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>                                                                                                                                                
#include <boost/asio.hpp>
#include <string>
#include "DS.h"

int main()
{
  StatusCode sc;
  char proto_msg[DS_CHAR_BUF_SIZE], repl_msg[DS_CHAR_BUF_SIZE];
  memset(proto_msg, 0, DS_CHAR_BUF_SIZE);
  memset(repl_msg, 0, DS_CHAR_BUF_SIZE);
  DS_Manager mgr;
  cout << "Starting server thread" << std::endl;
  //std::thread(DS_RemoteManager::spawnServer, 7001, mgr.rm).detach();
  mgr.rm->spawnServer(7010, mgr.rm);
  // DS_FileHandle hdl;
  // mgr.createRemoteFile("heythere", "10.50.40.251", "7001");
  // mgr.loadFile("buffermgr.cc", hdl);
  // DS_PageHandle ph;
  // hdl.getThisPage(2, ph);
  //DS_FileHandle hdl2;
  // mgr.createRemoteFile("iuiu", "127.0.0.1", "7003");
  // mgr.createRemoteFile("file2", "32.43.4.34", "7009");
  // mgr.loadFile("DS_RemoteManager.cc", hdl);
  // cout << "curr: " << hdl.hdr.firstFree << std::endl;
  // cout << "dirr: " << hdl.hdr.numPages << std::endl;
  // DS_PageHandle ph;
  // hdl.getThisPage(1, ph);
  // int choice;
  // char name[100];
  // char ipaddr[100];
  // char portj[100];
  // int pnum;

  // do {
  //   cout << "Welcome to Distributed Storage" << std::endl;
  //   cout << "Enter choice: \n1. Create File\n2. Create Remote File\n3. Load Page\n4. Exit" << std::endl;
  //   cin >> choice;
  //   cout << "Enter name: ";
  //   cin >> name;
  //   cout << endl;
  //   if (choice == 1) {
  //     mgr.createFile(name);
  //   }
  //   else if (choice == 2) {
  //     cout << "Enter IP: ";
  //     cin >> ipaddr;
  //     cout << endl;
  //     cout << "Enter port: ";
  //     cin >> portj;
  //     cout << endl;
  //     mgr.createRemoteFile(name, ipaddr, portj);
  //   }
  //   else if (choice == 3) {
  //     cout << "Enter page num: ";
  //     cin >> pnum;
  //     cout << endl;
  //     DS_PageHandle ph;
  //     DS_FileHandle fh;
  //     mgr.loadFile(name, fh);
  //     fh.getThisPage(pnum, ph);
  //   }
  // } while (choice != 4);

  return 0;
}
